#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <bitset>
#include <functional>

using namespace std;

struct repeatable_ifstream: public ifstream 
{
  repeatable_ifstream( string fn ): ifstream(fn), name(fn) {}
  repeatable_ifstream& operator=( repeatable_ifstream&& ) = default;
  
  void reset() {
    ifstream::close();
    operator=( repeatable_ifstream(name) ); 
  }
  inline void close(){ reset(); }

  private: 
  string name;
};

typedef repeatable_ifstream rifstream;
                            
template<typename T>
ifstream& operator>>( ifstream& in, vector<T>& v )
{
  while( !in.eof() ){ 
    T e;
    in >> e;
    v.push_back(e);
  } 
  in.close();
}


int main() {

//  rifstream in("INPUT");
  rifstream in("EG_1");
//  rifstream in("EG_2");
//  rifstream in("EG_3");  

//  using T = bitset<12>;
  using T = bitset<5>;

  vector<T> v;
  in >> v;

  T gama;
  generate( gama.begin(), gama.end(), 
    [&v,i=-1]() mutable { 
      ++i;
      return accumulate(v.begin(), v.end(), 0, 
        [&i]( const size_t sum, const auto& elm ) -> size_t
        {
          return sum + elm[i];
        }) > v.size()/2; 
    });

  cout <<  gama * ~gama  << endl;
}
