#include <iostream>
#include <iterator>
#include <sstream>
#include <fstream>

#include <string>
#include <vector>
#include <set>
#include <algorithm>
#include <numeric>


using namespace std;


//    h--------------------------------------





int main() 
{
//  j----------------------------------------
  fstream in("INPUT");
//  const size_t N = 100;

//  fstream in("EG_1");
//  const size_t N = 100;

  using T = uint64_t;
  vector<T> vec(9, 0);


  T t;
  while ( !in.eof() )
  {
    in >> t;
    in.ignore();
    ++vec[t];
  }
  --vec[t];

  cerr << "[ "; 
  copy( vec.begin(), --vec.end(), ostream_iterator<int>(cerr, ", ") );
  cerr << vec.back() << "]" << endl;

  for( int i = 0; i < 256; ++i )
  {
    rotate( vec.begin(), ++vec.begin(), vec.end() );
    vec[6] += vec[8];
  }

  cerr << "[ "; 
  copy( vec.begin(), --vec.end(), ostream_iterator<T>(cerr, ", ") );
  cerr << vec.back() << "]" << endl;

  cout << accumulate(vec.begin(), vec.end(), 0ull ) << endl; 

}
