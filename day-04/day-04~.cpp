#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <sstream>

using namespace std;

using T = int;

ostream& operator<<( ostream& out, const vector<vector<T>>& m )
{
  for ( int i =0; i<5; ++i)
  {
    copy( m[i].begin(), m[i].end(), ostream_iterator<T>(out, " "));
    out << endl;
  }
  return out;
}


int main() {
  ifstream in("INPUT");
//  ifstream in("EG_1");
//  ifstream in("EG_2");
//  ifstream in("EG_3");
  vector<T> r;
  vector<vector<vector<T>>> m;
 


  T t;
  string s;
  stringstream ss;

  in >> s;
  ss = stringstream(s);

  while ( ss >> t ) 
  {
    ss.ignore();
    r.push_back(t);
  }
  copy(r.begin(), r.begin() + 10, ostream_iterator<int>(cerr, ", ") );

  while( !in.eof() )
  {
    in.ignore();
    vector<vector<int>> b(5);

    generate( b.begin(), b.end(), 
      [&in]() mutable { 
        vector<int> row; 
        for ( int _ =0; _ < 5; ++_)
        {
          int t;
          in >> t; in.ignore();
          row.push_back(t);
        }
        return row; 
      });
    m.push_back(b);
  }

  m.pop_back();

  cerr << m.size() << endl;
  for ( int _ =0; _ < 3; ++_) cerr << m[_] << endl;

  
  vector<int> w(m.size());
  transform( m.begin(), m.end(), w.begin(), 
      [&,i=0](const auto& e) mutable{

        vector<vector<int>> _e(5);
        transform( e.begin(), e.end(), _e.begin(), 
            [&] ( const auto& j ) {

              vector<int> _j(5);
              transform(j.begin(), j.end(), _j.begin(), 
                [&]( const auto& elm )
                {
                  return find( r.begin(), r.end(), elm ) - r.begin();
                });
              return _j;
            });

        if ( ++i < 3 ) 
          cerr << _e << endl;


        vector<int> k(10);
        transform(_e.begin(), _e.end(), k.begin(), 
          [&]( const auto& elm )
          {
            return *max_element(elm.begin(), elm.end());
          });

        generate( k.begin()+5, k.end(), 
          [&,_i=-1] () mutable{ 
            int M = 0;
            ++_i;
            for( int i = 0; i < 5; ++i )
            {
              if (M < _e[i][_i]) M = _e[i][_i];
            }
            return M; 
          });

        if ( i < 3 ) { 
          copy( k.begin()+5, k.end(), ostream_iterator<int>(cerr, ", ") );
          cerr << endl << endl;
        }

        return *min_element(k.begin(), k.end());
      });

  const auto a=max_element(w.begin(),w.end());
  const auto ra=r.begin() + *a + 1;
  cerr << "winner is #" << a - w.begin() << endl;




  const size_t val = accumulate( m[a - w.begin()].begin(), m[a - w.begin()].end(), 0, 
      [&]( const size_t s, const auto raw ){
        return s + accumulate(raw.begin(), raw.end(), 0, 
          [&]( const size_t sum, const auto& elm ) -> size_t
          {
            return sum + ( find(r.begin(), ra,elm) == ra ? elm : 0); 
          });
      });



  cout <<  "value: " <<  val * *(ra-1) << endl;
}
