#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>

#include "aoc_algorithm.h"

using namespace std;


//template<class It1, class It2>
//It1 find_last_of( It1 first, It1 last, It2 s_first, It2 s_last ) 
//{
//  vector<typename iterator_traits<It2>::value_type> tmp_ = { s_first, s_last }; 
//  auto ie = tmp_.end();
//  while( tmp_.begin() != ie && first != last )
//  {
//    ie = remove_if(tmp_.begin(), tmp_.end(), 
//      [&]( const auto& elm ) -> bool
//      {
//        return *first == elm;
//      }),
//    ++first;
//  }
//  return first;
//}

template<typename T, size_t N>
struct board: public array< array<T, N>, N>
{
  using array_t = array< array<T, N>, N>;
  using array_t::begin;
  using array_t::end;

  board<T,N> flip() const 
  {
     board<T,N> ret; 
     for( int i = 0; i < N; ++i )
     {
        for( int j = 0; j < N; ++j )
        {
          ret[i][j] = array_t::operator[](j)[i];
        }
     }
    return ret;
  }

  template<typename It, bool flipped = false>
  It victory( It bn, It ed ) const
  {
    for_each( begin(), end(), 
    [&]( auto elm ) -> void  //cpy
    {
      ed = find_last_of(bn, ed, elm.begin(), elm.end());
    });
    if constexpr ( flipped )
      return ed;
    else
      return flip().template victory<It, true>( bn, ed );
  }

  template<typename It>
  size_t count( It bn, It ed )
  {
    return accumulate(begin(), end(), 0, 
      [&]( const size_t sum, const auto& elm ) -> size_t
      {
        return accumulate(elm.begin(), elm.end(), sum, 
          [&]( const size_t sum_, const auto& elm_ ) -> size_t
          {
            return sum_ + (find(bn, ed, elm_) == ed ? elm_ : 0);
          });
      });
  }
};

template<typename T, size_t N>
istream& operator>>( istream& in, board<T,N>& b )
{
  for_each(b.begin(), b.end(), 
    [&]( auto& elm ) -> void 
    {
      generate( elm.begin(), elm.end(), 
        [&](){ 
          T t; 
          in >> t;
          return t; 
        });
      in.ignore();
    });
  return in;
}


template<typename T, size_t N>
ostream& operator<<( ostream& out, const board<T,N> m )
{
  for ( int i =0; i<5; ++i)
  {
    copy( m[i].begin(), m[i].end(), ostream_iterator<T>(out, " "));
    out << endl;
  }
  return out;
}






int main() {
  ifstream in("INPUT");
  using T = board<int, 5>; 
//  ifstream in("EG_1");
//  ifstream in("EG_2");
//  ifstream in("EG_3");

  vector<int> R;
  vector<T> M;

  int t;
  while ( in >> t, R.push_back(t), in.get() == ',' );

  cerr << "R ["; 
  copy_n( R.begin(), 10, ostream_iterator<int>(cerr, ", ") );
  cerr << "... ]" << endl;

  while( in.ignore(), !in.eof() )
  {
    T b;
    in >> b;
    M.push_back(b);
  }
//  m.pop_back();

  cerr << "M ["; 
  copy_n( M.begin(), 10, ostream_iterator<T>(cerr, "\n") );
  cerr << "... ]" << endl;

  
  vector<int> W(M.size());

  transform(M.begin(), M.end(), W.begin(), 
    [&]( const auto& elm )
    {
      return elm.victory( R.begin(), R.end() ) - R.begin();
    });

  const auto a=max_element(W.begin(),W.end());

  const auto ra=R.begin() + *a;

  const auto val=M[a - W.begin()].count(R.begin(), ra);

  cerr << "winner is #" << a - W.begin() << endl;
  cerr << M[a - W.begin()] << endl;

  cerr << "winner count:" << val << endl;

  cout <<  "value: " <<  val * *(ra-1) << endl;
}
