#include <iostream>
#include <fstream>

#include <string>
#include <vector>

#include <ranges>
#include <algorithm>
#include <functional>

#include <execution>
#include <cmath>

using namespace std;
namespace rn = ranges;


template<typename T>
struct instruction_t
(
);


template<typename T>
struct monad_t
{
  bool operator( vector<uint8_t> &&fig ) { 
    for( instruction_t t : instructs )  
      t( *this, fig );
    return z==0; 
  }

  vector<instruction_t> instructs;
  T x,y,z,w;
};


template<typename T>
istream& operator>>(istream& in, monad_t<T>& m )
{
  rn::copy( views::istream<instruction_t>,  back_inserter(m.instructs) );
}


constexpr int N_THREAD = 16; 
constexpr int PERCENT_TARGET = 10; 

constexpr auto gtor( const uint32_t a ) {
  return [=]() mutable { uint8_t d = a % 10; a /= 10; return d; };
} 

void zeroless( vector<uint8_t> a ) 
{
  auto it = rn::find( a, 0 );
  if ( it == a.end()  ) return;
  assert( it != a.begin() );
  *--it -= 1;
  fill( ++it, a.end(), 9 );
} 

int main() {

  monad_t<long> monad;
  ifstream("INPUT") >> monad;

  using split_t = tuple<uint8_t, uint32_t, uint32_t>;

  vector<pair<uint32_t, uint32_t>> t_rn( N_THREAD ); 
  rn::generate( t_rn, 
      [ i=0,
        M=pow(10,15)-1ul, 
        N=(uint32_t)(M*(100 - PERCENT_TARGET)/ 100),
        R=(uint32_t)(N/ N_THREAD) ] () mutable 
      -> split_t 
      {
        uint32_t m = M;
        M -= R;
        return {i++, m, M };
      });

  vector<uint32_t> rv( N_THREAD, 0 );
  find_if( execution::par, t_rn.begin(), t_rn.end(), 
      [&]( const split_t& r ){
        vector<uint8_t> rb(14), re(14);
        rn::generate( rb | reverse, gtor( get<1>(r) ) );
        rn::generate( re | reverse, gtor( get<2>(r) ) );
        zeroless( rb );
        zeroless( re );
        auto it = rb.begin();
        auto jt = re.begin();
        while ( it != rb.end() )
        {
          auto ut = rb.rbegin();
          while ( *ut <= 1 )
          {
            *ut = 9;
            ++ut; 
          }
          --(*ut);
          tie( it, jt ) = mismatch( it, rb.end(), jt, re.end() );
          if ( monad( rb ) )
          {
            rv[ get<0>(i) ] = accumulate(rb.begin(), rb.end(), 0ul, 
              [&]( const uint32_t sum, const uint8_t elm ) -> uint32_t
                { return sum * 10 + elm; }); 
            return true
          }
        }
        return false;
      });
}
