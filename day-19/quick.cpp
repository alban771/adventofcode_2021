#include <iostream>
#include <ranges>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;
namespace rn = ranges;





int main() {



  cerr << "join view" << endl;

  vector<vector<int>> back = {{0,1,2}, {3,4,5}, {6,7,8}};
  auto v = rn::join_view( back );


  cerr << rn::distance( v ) << endl; 
  cerr << rn::distance( v.begin(), v.end() ) << endl; 



  vector<vector<int>> lazy{3};
  rn::generate( lazy, []{
      vector<int> rv = {1,2,3};
      return rv;
      });
  auto w = rn::join_view( lazy );

  cerr << rn::distance( w ) << endl; 
  cerr << rn::distance( w.begin(), w.end() ) << endl; 



  cerr << "permutationss (03" << endl;

  vector<int> h(3);
  iota( h.begin(), h.end(), 0 );


  do {
  cerr << "[ "; 
  copy( h.begin(), --h.end(), ostream_iterator<int>(cerr, ", ") );
  cerr << h.back() << "]" << endl;
  } 
  while( next_permutation(h.begin(), h.end()));

  


}
