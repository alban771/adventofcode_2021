#include <iostream>
#include <fstream>

#include <algorithm>
#include <numeric>
#include <ranges>
#include <iterator>

#include <tuple>
#include <vector>
#include <set>
#include <map>

#include <cmath>
#include <cassert>



using namespace std;
namespace rn = ranges;



struct pos_t : public vector<int> 
{
  pos_t(): vector<int>{0,0,0} {}
  pos_t( vector<int>&& a ): vector<int>(move(a)) {}


  pos_t operator+( const pos_t& oth ) const {
    return vector<int>{ at(0) + oth[0], at(1) + oth[1], at(2) + oth[2] };
  }

  pos_t operator-( const pos_t& oth ) const {
    return vector<int>{ at(0) - oth[0], at(1) - oth[1], at(2) - oth[2] };
  }

  pos_t operator*( const pos_t& oth ) const {
    return vector<int>{ at(0) * oth[0], at(1) * oth[1], at(2) * oth[2] };
  }

  operator size_t () const {
    return abs(at(0))+abs(at(1))+abs(at(2));
  }

  bool operator<( const pos_t& oth ) const {
    return rn::lexicographical_compare( *this, oth, less<int>() );
  }

  pos_t& operator>>( uint8_t i ) {
    i %= size();
    rotate( begin(), begin() + size()-i, end() );
    return *this;
  }
};





struct scanner_t: public vector<pos_t>
{
  using vector<pos_t>::size;
  using vector<pos_t>::begin;
  using vector<pos_t>::end;
  using vector<pos_t>::at;


public:
  vector<vector<pair<size_t,int>>> distances;
  int id, n_perm = 0;
  pos_t pos;

public:

  // compute the distance matrix
  void make_dist() {
    distances.resize(size());

    rn::generate( distances, 
      [&,i=0]() mutable 
      {
        vector<pair<size_t,int>> elm{size()-1};
        rn::generate( elm, 
        [&,j=0]() mutable -> pair<size_t,int> 
        {
          if ( i == j ) ++j;
          size_t d = (*this)[i] - (*this)[j];
          return {d, j++};
        });
        rn::sort(elm, {}, &pair<size_t, int>::second);
        ++i;
        return elm;
      });
  }



  tuple<int, vector<pos_t>, int, vector<pos_t>> operator/( const scanner_t& oth ) const {

    cerr << "sc: " << id << " / sc: " << oth.id << " -> "; 

    int a,b,c,o_a,o_b,o_c;
    vector<pair<size_t,int>> comm_a, o_comm_a;

    auto f = rn::find_if( distances, [&]( auto& elm ){
        auto f_ = rn::find_if( oth.distances, [&]( auto& o_elm ) {
            comm_a.clear();
            o_comm_a.clear();
            rn::set_intersection( elm, o_elm, back_inserter(comm_a), {}, 
                &pair<size_t, int>::first, &pair<size_t, int>::first);
            rn::set_intersection( o_elm, elm, back_inserter(o_comm_a), {}, 
                &pair<size_t, int>::first, &pair<size_t, int>::first);
            return comm_a.size() > 1;
          });
        o_a = distance( oth.distances.begin(), f_ ); 
        return f_ != oth.distances.end();
      });
    a = distance( distances.begin(), f );

    if ( f == distances.end() ) 
    {
      cerr << "no A" << endl;
      return {};
    }


    bool g = rn::any_of( comm_a, 
      [&,i=0](const auto& p_b) mutable -> bool
      {
        b = p_b.second;
        o_b = o_comm_a[i].second;
        return rn::any_of( comm_a | views::drop(i+1), 
            [&,j=i](const auto& p_c) mutable
            {
              c = p_c.second;
              o_c = o_comm_a[++j].second;
              return (*this)[b] - (*this)[c] == oth[o_b] - oth[o_c];
            });
      });

    if ( !g )
    {
      cerr << "no B, C" << endl;
      return {};
    }

    vector<pos_t> rv { (*this)[a] };
    vector<pos_t> o_rv { oth[o_a] };
    for( int i = 0; i < comm_a.size(); )
    {
      int e = comm_a[i].second, o_e = o_comm_a[i].second;
      if( (*this)[e] - (*this)[b] == oth[o_e] - oth[o_b] &&
          (*this)[e] - (*this)[c] == oth[o_e] - oth[o_c] )
      {
        rv.push_back( (*this)[e] );
        o_rv.push_back( oth[o_e] );
      }
    }

    cerr << "#" << rv.size() << endl;
    return { a, rv, o_a, o_rv };
  }



  bool operator/=( scanner_t&& oth ) {
    const auto [ a, s, o_a, o_s ] = *this / oth;

    if ( s.size() == 0 )
      return false;

    vector<pos_t> o_from_a { oth.size()-1 }; 
    rn::transform( oth.distances[o_a], o_from_a.begin(), 
      [&]( const auto& elm ) { return oth[elm.second]; });

    vector<pos_t> o_diff { oth.size() - s.size() };
    rn::set_difference( o_from_a, o_s, o_diff.begin(), 
        []( const pos_t& lhs, const pos_t& rhs ){ 
            return rn::equal( lhs, rhs );
          });



    pos_t ab = s[1] - s[0];
    pos_t o_ab = o_s[1] - o_s[0];
    int i = 1;

    while( rn::any_of(ab, [](const int k){ return k == 0;}) ) 
    {
      ++i;
      assert( i < s.size() ); //no luck, bad conditioning

      rn::transform( ab, ab.begin(), 
          [&,j=-1]( const int k) mutable{ 
            ++j;
            if ( k != 0 )
              return k;
            return s[i][j] - s[0][j];
          })
      rn::transform( o_ab, o_ab.begin(), 
          [&,j=-1]( const int k) mutable{ 
            ++j;
            if ( k != 0 )
              return k;
            return o_s[i][j] - o_s[0][j];
          })
    }

    i = 0;

    while( rn::mismatch(ab, o_ab, {}, abs, abs) )
    {
      ++i;
      assert( i < ab.size() ); //err, distance collision

      o_ab >> 1;
    }

    const pos_t ori = ab / o_ab;
    const pos_t offst = s[0] - (o_s[0] >> i) * ori; 

    assert( rn::count( ori, -1 ) % 2 == 0 ); //direct referentials

    rn::transform( o_diff, o_diff.begin(), [&]( const pos_t& l ){
          return (l >> i) * ori + offst;
        });

    assert( rn::all_of( o_diff, [](const pos_t& p) {
          return rn::any_of( p, []( const int k ){ return abs(k) > 999; }); 
        })); //new points are out of reach


    rn::copy( o_diff, back_inserter(*this) );
    make_dist();
    return true;
  }


};



//
//
//  pos_t operator&&( const set<pos_t>& pos ) const {
//    if ( pos.size() < 3 )
//    
//
//      cerr << "not enough points" << endl;
//      throw "";
//    }
//
//    const auto t_dist = rn::join_view(dist); 
//    vector<pos_t> p_(3);
//+    copy_n( pos.begin(), 3, p_.begin() );
//
//    const auto d0 = rn::find( t_dist, p_[0]-p_[1] );
//    const auto d1 = rn::find( t_dist, p_[0]-p_[2] );
//    const auto [a,b] = index[ rn::distance(t_dist.begin(), d0) ];
//    const auto [c,d] = index[ rn::distance(t_dist.begin(), d1) ];
//    if ( a == c || a == d )
//    {
//      assert( b != c && b != d);
//      turn p_[0] + -at(a); 
//    }
//    else if ( b == c || b == d )
//    {
//      return p_[0] + -at(b); 
//    }
//    else
//    {
//      cerr << "ambiguous distance" << endl;
//      throw "";
//    }
//  }
//    const auto o = rn::find( o_dist, *t );
//    const auto [c,d] = oth.index[rn::distance(o_dist.begin(), o)];
//    return {{a,b},{c,d}};
//  }

//
//
//
//  // insert one point to the similar mesh 
//  bool mesh_next( const scanner_t& oth, dual_mesh_t& mesh ) const {
//    auto& [t_mesh, o_mesh] = mesh;
//    vector<int> t_pos(size() - 2), o_pos(oth.size() - 2);
//    t_pos.erase( rn::set_difference( views::iota(0ul, size()), 
//          t_mesh, t_pos.begin()).out, t_pos.end());
//    o_pos.erase( rn::set_difference( views::iota(0ul, oth.size()), 
//          o_mesh, o_pos.begin()).out, o_pos.end());
//
//    const auto get_dist = [&]( const scanner_t& sc, const int i ){
//      return [&sc,i](const int j){ 
//          return i < j ? sc.dist[i][j-i-1] : sc.dist[j][i-j-1];
//      };
//    };
//
//    auto jt = o_pos.begin();
//    const auto it = rn::find_if( t_pos, 
//        [&]( const int i ){
//          vector<size_t> t_dist;
//          rn::transform(t_mesh, back_inserter(t_dist), get_dist( *this, i ));
//          jt = rn::find_if( o_pos, 
//              [&]( const int j ){
//                return rn::all_of( o_mesh | views::transform(get_dist(oth, j)),
//                    [&]( const size_t d ){ 
//                      return rn::find( t_dist, d ) != t_dist.end(); 
//                    });
//              });
//          return jt != o_pos.end();
//        });
//
//    if ( it == t_pos.end() )
//      return false;
//
//    t_mesh.insert( *it );
//    o_mesh.insert( *jt );
//    return true;
//  }
//
//
//  // construct the similar mesh incrementaly  
//  dual_mesh_t operator/(const scanner_t& oth ) const {
//    dual_mesh_t rv = mesh_init( oth );
//    if ( get<0>(rv).size() > 0 )
//      while( mesh_next( oth, rv ) );
//    return rv;
//  }


  // align with the given offset
//  void operator+=( const size_t& d ) {
//    for_each( begin(), end(), [&d]( pos_t& a) { a += d; });
//  }


//  vector< vector<size_t> > common_dist_by_ref( const scanner_t& oth ) {
//
//    vector< vector<size_t> > rv(6);
//    generate( rv.begin(), rv.end(), 
//      [&](){ 
//        for_each( dist.begin(), dist.end(), 
//            []( auto& elm ){
//              next_permutation( get<2>(elm).begin(), get<2>(elm).end() );
//            });
//        return common_dist<false>( oth ); 
//      });
//    return rv;
//  }


//  vector<pos_t> beacon_from_dist( const vector<size_t>& d ) {
//
//    vector<pos_t> rv;
//    set<int> pos_index;
//
//    for_each(d.begin(), d.end(), 
//      [&]( auto& d_ ) mutable -> void 
//      {
//        auto it = find_if( dist.begin(), dist.end(), [&d_]( const auto& elm ){ 
//            return equal( get<2>(elm).begin(), get<2>(elm).end(), 
//                d_.begin() );
//        });
//        if ( it == dist.end() )
//          cerr << "distance not found " << endl;
//        else {
//          const auto [i,j,_] = *it;
//          pos_index.insert( i );
//          pos_index.insert( j );
//        }
//      });
//
//    transform(pos_index.begin(), pos_index.end(), back_inserter(rv), 
//      [&]( const auto& elm )
//      {
//        return vector<pos_t>::at(elm);
//      });
//
//    return rv;
//  }
istream& operator>>(istream& in, pos_t& p) 
{
  int x,y,z;
  char c;
  in >> x >> c >> y >> c >> z;
  in.ignore();
  p = vector<int>{x,y,z};
  return in;
}

ostream& operator<<(ostream& out, const pos_t& p) {
  return out << "(\t" << p[0] << " ,\t" << p[1] << " ,\t" << p[2] << " )";
}

istream& operator>>(istream& in, scanner_t& sc) {
  string s;
  in >> s >> s >> sc.id >> s;
  while( !in.fail() )
  {
    pos_t p;
    in >> p;
    sc.push_back(p);
  }
  sc.pop_back();
  sc.make_index();
  sc.make_dist();
  in.clear();
  in.ignore();
  return in;
}

ostream& operator<<(ostream& out, const scanner_t& sc) {
  out << "[ "; 
  copy( sc.begin(), --sc.end(), ostream_iterator<pos_t>(out, "\n") );
  return out << sc.back() << "]" << endl;
}



int main()
{
  ifstream in("eg1");
  vector<scanner_t> vec;

  while( !in.eof() )
  {
    scanner_t t;
    in >> t;
    vec.push_back( t );
  }

  cerr << "#" << vec.size() << endl;

  while ( vec.size() > 1 )
  {
    const auto f = rn::find_if( vec,  
      [&, i=0]( scanner_t& sc_a ) mutable -> bool
      {
        const auto rmv = remove_if( vec.begin() + ++i, vec.end(), 
            [&sc_a]( auto& sc_b ) -> bool
            {
              return rn::any_of( views::iota(0,6), [&]( const int _ )
                  {
                    sc_b >> 1;
                    return sc_a /= move(sc_b);
                  });
            });

        if ( rmv != vec.end() )
          vec.erase( rmv, vec.end());

        return rmv != vec.end();
      });


    cerr << "#" << vec.size() << endl;
    assert( f != vec.end() );
  }

  cout << vec[0].size() << endl;

}
