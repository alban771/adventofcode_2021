#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>

using namespace std;

using T = int;


int main() {
  ifstream in("INPUT");
  vector<T> v;
  T _, a = 0, d = 0, f = 0;


  while ( !in.eof() ){
    string w;
    in >> w; in.ignore(); in >> _; 
    if ( w == "forward")
    {
      f += _;
      d += a * _;
    }

    if ( w == "up")
      a -= _;
      
    if ( w == "down")
      a += _;
  }
  

  cout <<  d * f  << endl;
}
