#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <string>
#include <iterator>
#include <algorithm>
#include <numeric>

using namespace std;


const auto digit_match( const vector<char>& perm ) {
  return [&](const string_view d, const string& s){
    if ( d.size() != s.size() ) 
      return false;
    return all_of( d.begin(), d.end(), 
        [&](const char c){ 
          return s.find(perm[c - 'a']) != string::npos;
        });
  };
} 


struct entry
{
  static constexpr array<string_view, 10> digits = {"abcefg", "cf", "acdeg", "acdfg", "bcdf", 
    "abdfg", "abdefg", "acf", "abcdefg", "abcdfg"};



  vector<string> pattern;
  vector<string> out;

  bool test( const vector<char> & p )
  {
    const auto d_match = digit_match( p );
    bool ret = all_of( pattern.begin(), pattern.end(), 
        [&]( const string& s) {
          return find_if( digits.begin(), digits.end(), 
              [&](const string_view d ){
                return d_match( d, s );
              }) != digits.end();
        });
    if ( ret ) perm = p;
    return ret; 
  }


  size_t value() const {
    vector<size_t> val(4);
    const auto d_match = digit_match( perm );
    transform( out.begin(), out.end(), val.begin(), 
        [&](const string& s)
        {
          return find_if( digits.begin(), digits.end(), 
              [&](const string_view d){
                return d_match(d, s);
              }) - digits.begin();
        });
    return accumulate(val.begin(), val.end(), 0ll, 
      [&]( const size_t sum, const auto& elm ) -> size_t
      {
        return sum * 10 + elm;
      });
  }


 private:
  vector<char> perm;
};

istream& operator>>( istream& in, entry& e )
{
  string t;
  e.pattern.resize(10);
  e.out.resize(4);
  generate( e.pattern.begin(), e.pattern.end(), 
    [&](){ 
      string t;
      in >> t;
      in.ignore();
      return t; 
    });
  in.ignore();
  in.ignore();
  generate( e.out.begin(), e.out.end(), 
    [&](){ 
      string t;
      in >> t;
      in.ignore();
      return t; 
    });
  return in;
}

ostream& operator<<(ostream& out, const entry& e)
{
  copy( e.pattern.begin(), e.pattern.end(), ostream_iterator<string>(out, " "));
  out << "| ";
  copy( e.out.begin(), e.out.end(), ostream_iterator<string>(out, " "));
  return out;
}



int main() {


  fstream in("INPUT");
//  fstream in("EG");
  using T = entry;

  vector<T> vec;

  while (!in.eof())
  {
    T t;
    in >> t;
    vec.push_back(t);
    if ( in.peek() == '\0' )
      in.ignore();
  }

  cerr << "[ "; 
  copy_n( vec.rbegin(), 10, ostream_iterator<entry>(cerr, "\n") );
  cerr << "... ]" << endl;


//  cout << accumulate(vec.begin(), vec.end(), 0, 
//    []( const size_t sum, const auto& elm ) -> size_t
//    {
//      return sum + count_if(elm.out.begin(), elm.out.end(), 
//          [](const string& s){ 
//            return s.size() <=4  || s.size() == 7;
//          });
//    }) << endl;
//
  vector<char> perm(7);

  iota(perm.begin(), perm.end(), 'a');

  auto it = vec.begin();
  do
  {
    it = partition(it, vec.end(), 
        [&](entry& e){ 
          return e.test( perm );
        });
  }
  while( next_permutation(perm.begin(), perm.end()) );

  cout << accumulate(vec.begin(), vec.end(), 0ll, 
    [&]( const size_t sum, const auto& elm ) -> size_t
    {
      return sum + elm.value();
    }) << endl;
}
