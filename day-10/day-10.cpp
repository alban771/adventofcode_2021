#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include <utility>
#include <functional>
#include <iterator>
#include <numeric>
#include <complex>


using namespace std;



int main() {

//  fstream in("EG");
  fstream in("INPUT");
  using T = string;
  vector<T> vec;


  while(  !in.eof())
  {
    T t;
    in >> t;
    vec.push_back(t);
  }
  vec.pop_back();

  cerr << "[ "; 
  copy_n( vec.rbegin(), 2, ostream_iterator<T>(cerr, "\n") );
  cerr << "... ]" << endl;


  vector<vector<char>> cs(vec.size());

  transform( vec.begin(), vec.end(), cs.begin(), 
      []( const string& s ){ 
        vector<char> q;
        auto it = find_if(s.begin(), s.end(), 
          [&q]( auto elm ) -> bool 
          {
            if ( "[{<("s.find(elm) != string::npos )
            {
              q.push_back(elm);
              return false;
            }
            else
            {
              char c = q.back();
              q.pop_back();
              return  (c == '(' && ')' != elm) ||
                      (c == '{' && '}' != elm) || 
                      (c == '[' && ']' != elm) || 
                      (c == '<' && '>' != elm) ;
            }
          });
        return it == s.end() ? q : vector<char>{}; 
      });

  cs.erase( 
    remove_if(cs.begin(), cs.end(), 
      [&]( const auto& elm ) -> bool
      {
        return elm.size() == 0;
      }),
    cs.end());
  


  cerr << "[ "; 
  copy( cs.back().begin(), --cs.back().end(), ostream_iterator<char>(cerr, ", ") );
  cerr << cs.back().back() << "]" << endl;


//  const map<char, size_t> values = {{'_',0},{')',3},{']',57},{'}',1197},{'>',25137}};
  const map<char, uint64_t> values = {{'(',1},{'[',2},{'{',3},{'<',4}};

  vector<uint64_t> sc(cs.size());
  transform(cs.begin(), cs.end(), sc.begin(), 
    [&]( const auto& elm )
    {
      return accumulate(elm.rbegin(), elm.rend(), 0ull, 
        [&]( const uint64_t sum_, const auto elm_ ) -> size_t
        {
          return 5*sum_ + values.at(elm_);
        });
    });
  sort( sc.begin(), sc.end());

  cerr << "[ "; 
  copy( sc.begin(), --sc.end(), ostream_iterator<uint64_t>(cerr, ", ") );
  cerr << sc.back() << "]" << endl;


  cout << sc[ sc.size() / 2 ] << endl;

}
