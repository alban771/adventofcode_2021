#include <iostream>
#include <fstream>

#include <vector>
#include <sstream>
#include <bitset>
#include <memory>

#include <iterator>
#include <numeric>
#include <functional>

using namespace std;


using header_t = bitset<6>;
static constexpr header_t TYPE {0x7};
static constexpr header_t VALUE {0x4};


struct payload_t {
  virtual ~payload_t() = default;
  virtual uint64_t operator*() { return 0; };
  virtual istream& read( istream& ) = 0;
  size_t len = 0;
};


struct packet_t 
{
  template<typename R>
  R visit( const function<R(const packet_t&)>& f ) const {
    return accumulate( sub_pk.begin(), sub_pk.end(), f(*this), 
        [&]( const R sum, const packet_t& p_ ) {
          return sum + p_.visit( f );
        });
  }

  operator uint64_t() const {
    switch( (h & TYPE).to_ulong()  ) 
    {
      case 0ul:
        return accumulate(sub_pk.begin(), sub_pk.end(), 0ull );
      case 1ul:
        return accumulate(sub_pk.begin(), sub_pk.end(), 1ull, multiplies<>());
      case 2ul:
        return *min_element(sub_pk.begin(), sub_pk.end() );
      case 3ul:
        return *max_element(sub_pk.begin(), sub_pk.end() );
      case 4ul:
        return **p;
      case 5ul:
        return sub_pk.front() > sub_pk[1];
      case 6ul:
        return sub_pk.front() < sub_pk[1];
      case 7ul:
        return sub_pk.front() == sub_pk[1];
    }
    return 0;
  }

  header_t h;
  unique_ptr<payload_t> p;
  vector<packet_t> sub_pk; 
};

istream& operator>>( istream& in, packet_t& p );

struct value_t;

istream& operator>>( istream& in, value_t& v );


template<bool>
struct operator_t; 

template<bool Det>
istream& operator>>( istream& in, operator_t<Det>& op );




struct value_t: payload_t
{
  virtual ~value_t() = default;
  inline size_t operator*() { return n; }
  inline virtual istream& read( istream& in ){ return in >> *this; };
  uint64_t n; 
};

istream& operator>>( istream& in, value_t& v ) {
  v.n = 0;
  bitset<5> v_;
  do { 
    in >> v_;
    v.n <<= 4; 
    v.n += (v_ & bitset<5>{0xF}).to_ulong();
    v.len += 5;
  } while ( v_[4] );
  return in;
}



template<bool>
struct operator_t : payload_t {
  operator_t( packet_t& p ) : up(p) {};
  inline virtual istream& read( istream& in ){ return in >> *this; };
  packet_t &up;
};

template<bool Det>
istream& operator>>( istream& in, operator_t<Det>& op ) {

  bitset<15> l;
  in >> l;
  int n = l.to_ulong();
  op.len = 16 + n;
    
  while( n > 0 ) { 
    packet_t p;
    in >> p;
    n -= p.p->len + 6;
    op.up.sub_pk.push_back(move(p));
  }
  return in;
}


template<>
istream& operator>>( istream& in, operator_t<1>& op ) {

  bitset<11> l;
  in >> l;
  op.len = 12;

  op.up.sub_pk.resize(l.to_ulong());
  generate( op.up.sub_pk.begin(), op.up.sub_pk.end(), 
    [&]() { 
      packet_t p;
      in >> p;
      op.len += 6 + p.p->len;
      return p; 
    });
  return in;
}


istream& operator>>( istream& in, packet_t& p ){
  in >> p.h;
  if ( (p.h & TYPE) == VALUE )
    p.p = make_unique<value_t>();
  else if( in.get() == '0' )
    p.p = make_unique<operator_t<0>>( p );
  else
    p.p = make_unique<operator_t<1>>( p );
  return p.p->read(in);
}


int main() 
{
  string buf(8, 'x');
  ifstream in("INPUT");
  stringstream bin;

  while ( !in.eof() )
  {
    long n;
    stringstream tr; 
    size_t e = in.read( buf.data(), 8 ).gcount();
    tr << buf;
    tr >> hex >> n; 
    if ( e != 8 )
      n <<= 4*(8-e+1);
    bin << bitset<32>{ (uint32_t) n }.to_string();
  }

  packet_t pack;
  bin >> pack;

  const function<size_t(const packet_t&)> sum_version = []( const packet_t & p )
        { return (p.h >> 3).to_ulong(); };

  cout << pack.visit( sum_version ) << endl;
  cout << (uint64_t) pack << endl;
}
