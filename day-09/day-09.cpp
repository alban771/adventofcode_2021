#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
#include <functional>
#include <array>
#include <set>
#include <utility>


using namespace std;




using pt = pair<int, int>; 
using T = array< uint8_t, 100>;


istream& operator>>(istream& in, T& t)
{
  string s;
  in >> s;
  transform( s.begin(), s.end(), t.begin(), 
      [](const char c){ return (int)c - '0'; }); 
  return in;
}


ostream& operator<<( ostream& out, const pt& p )
{
}


int main() 
{

  fstream in("INPUT");
//  fstream in("EG");
  vector<T> vec;


  while( in.peek() != '\0' && !in.eof()){
    T t;
    in >> t;
    vec.push_back(t);
    in.ignore();
  }



  copy( vec.back().begin(), vec.back().end(), ostream_iterator<int>(cerr) );
  cerr << endl;


  vector<pt> pts;

  for( int i = 0; i < vec.size(); ++i )
  {
    for( int j = 0; j < vec[0].size(); ++j )
    {
      if( ( i == 0 || vec[i][j] < vec[i-1][j]  ) && 
          ( i+1 == vec.size() || vec[i][j] < vec[i+1][j] ) && 
          ( j == 0 || vec[i][j] < vec[i][j-1]  ) && 
          ( j+1 == vec[0].size() || vec[i][j] < vec[i][j+1]  ) ) 
        pts.push_back({i,j});
    }
  }

//  cerr << pts.size() << endl;
//  cerr << pts[0] << endl;
//  cerr << pts[1] << endl;
//  cerr << pts[2] << endl;

  vector<size_t> b(pts.size());
  transform(pts.begin(), pts.end(), b.begin(), 
    [&]( const auto& elm )
    {
      vector<pt> a = {elm};
      set<pt> b = {};
      while( a.size() > 0 )
      {
        const auto [i,j] = a.back();
        a.pop_back();
        if (vec[i][j] == 9) continue;

        vector<pt> n = {};
        if ( i > 0 ) 
          n.push_back({i-1, j});
        if ( i+1 < vec.size() ) 
          n.push_back({i+1, j});
        if ( j > 0 ) 
          n.push_back({i, j-1});
        if ( j+1 < vec[0].size() ) 
          n.push_back({i, j+1});

        const auto ni = partition(n.begin(), n.end(), 
            [&]( const auto& elm_ ) -> bool
            {
              return find( b.begin(), b.end(), elm_ ) != b.end();
            });

//        if ( any_of(n.begin(), ni, 
//          [&vec,i=i,j=j]( auto& elm_ ) -> bool 
//          {
//            auto [k,l] = elm_;
//            return vec[i][j] < vec[k][l]; 
//          }) )
//          continue;
          
        if ( any_of(ni, n.end(), 
          [&vec,i=i,j=j]( auto& elm_ ) -> bool 
          {
            auto [k,l] = elm_;
            return vec[i][j] > vec[k][l]; 
          }) )
          continue;
          
        b.emplace(i,j);
        copy( ni, n.end(), back_inserter(a) );
      }

      cerr << "[ ";
      for_each(b.begin(), b.end(), 
        [&]( auto& p ) -> void 
        {
          cerr << "(" << p.first << "," << p.second << ") ";
        });
      cerr << "]" << endl;
      
      return b.size();
    });


  sort(b.begin(), b.end());

  cerr << "[ "; 
  copy( b.begin(), --b.end(), ostream_iterator<size_t>(cerr, ", ") );
  cerr << b.back() << "]" << endl;

//  cout << accumulate(pts.begin(), pts.end(), 0, 
//    [&]( const size_t sum, const auto& elm ) -> size_t
//    {
//      const auto [i,j] = elm;
//      return sum + vec[i][j] + 1;
//    }) << endl;
//
  cout << accumulate(b.rbegin(), b.rbegin() + 3, 1ll, multiplies<size_t>()) << endl;

}
