#include <iostream>
#include <fstream>

#include <vector>
#include <map>
#include <tuple>
#include <string>
#include <set>
#include <bitset>

#include <algorithm>
#include <iterator>
#include <numeric>

#include <cmath>
#include <cassert>

using namespace std;


// INPUT
constexpr int start_p1 = 5;
constexpr int start_p2 = 6;

// EG
//constexpr int start_p1 = 4;
//constexpr int start_p2 = 8;


template< typename T, T Goal >
struct player 
{
  T pos = 0;
  T score = 0;

  player( const int start ): pos( start - 1 ) {}
  player( const player& ) = default;

  explicit operator bool() const {
    return score >= Goal;
  }

  void operator+=( const T die ) {
    pos += die;
    pos %= 10;
    score += pos + 1;
  }

  player operator+( const T die ) const {
    player p = *this;
    p += die;
    return p;
  }
};


struct determinist_die 
{
  uint8_t val = 100;
  uint64_t count = 0;

  int operator+( int i ) {
    count += i;
    if ( val + i <= 100 ) 
    {
      val += i; 
      return i*(val-i) +  i*(i+1)/2;
    }
    else
    {
      int j = 100 - val;
      val = i - j;
      return 100*j - j*(j-1)/2 + val*(val+1)/2;
    }
  }
};


template<typename Player>
struct quantum_die 
{
  constexpr static uint64_t P[7] = {1, 3, 6, 7, 6, 3, 1};

  quantum_die( Player ) {};

  tuple<uint64_t, uint64_t> roll( Player p1, const Player p2) {
    uint64_t wp1 = 0,  wp2 = 0;
    for( int i = 0; i < 7; ++i )
    {
      Player p = p1 + (3 + i); 
      if ( p )
      {
        wp1 += P[i]; 
        continue;
      }
      auto [ wp2_, wp1_ ] = roll(p2, p );
      wp1 += P[i] * wp1_;
      wp2 += P[i] * wp2_;
    }
    return { wp1, wp2 };
  }
};


int main() {

  player<uint16_t, 1000> p1_(start_p1), p2_(start_p2);
  determinist_die dd;
  while( !p2_ ) 
  {
    p1_ += dd+3;
    if ( p1_ ) break;
    p2_ += dd+3;
  }
  cout << min( p1_.score, p2_.score ) * dd.count << endl;


  player<uint8_t, 21> p1(start_p1), p2(start_p2);
  const auto [ wp1, wp2 ] = quantum_die(p1).roll( p1, p2 );
  cout << max( wp1, wp2 ) << endl;

}
