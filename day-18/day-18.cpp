#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>

#include <vector>
#include <bitset>
#include <math.h>


using namespace std;

struct snail_num;

ostream& operator<<( ostream& out, const snail_num& sn ); 

struct snail_num {
  vector<int> n, p;  
  snail_num() = default;
  snail_num( vector<int>&& n_, vector<int>&& p_ ): n(n_), p(p_) {}

  snail_num operator+( const snail_num& oth ) {
    vector<int> n_(n.size() + oth.n.size());
    vector<int> p_(n_.size());

    copy( n.begin(), n.end(), n_.begin());
    copy( oth.n.rbegin(), oth.n.rend(), n_.rbegin());
    copy( p.begin(), p.end(), p_.begin());
    copy( oth.p.rbegin(), oth.p.rend(), p_.rbegin());
    transform(p_.begin(), p_.end(), p_.begin(), 
      [&]( const int elm ) { return elm+1; });

    return snail_num(move(n_), move(p_)).reduce();
  }

  snail_num& reduce() {

    const auto explode = [&]( const size_t h ) {
      if ( h > 0 ) 
        n[h-1] += n[h];
      n[h] = 0;
      --p[h];
      if ( h < n.size() - 2 )
        n[h+2] += n[h+1];

      n.erase( next( n.begin(), h+1) );
      p.erase( next( p.begin(), h+1) );
    };

    const auto split = [&]( const size_t h ) {
      int val = n[h];
      n[h] /= 2;
      ++p[h];
      n.insert( next(n.begin(), h+1), (val+1) / 2 );
      p.insert( next(p.begin(), h+1), p[h] );
    };

//    cerr << *this << endl;
    while( true ) 
    {
      auto it = find_if( p.begin(), p.end(), 
              []( const int d ) { return d>4; } );
      if( it != p.end() ) 
      {
//        cerr << "e ";
        explode( distance( p.begin(), it ));
//        cerr << *this << endl;
        continue;
      }
      it = find_if( n.begin(), n.end(), 
              []( const int d ) { return d>9; } );
      if( it != n.end() ) 
      {
//        cerr << "s ";
        split( distance( n.begin(), it ));
//        cerr << *this << endl;
        continue;
      }
      break;
    }
//    cerr << endl;
    return *this;
  }

  size_t magnitude() {
    vector<int> i( n.size() );

    exclusive_scan( p.begin(), p.end(), i.begin(), 0,
        []( const int i_, const int p_ ) {
          return i_ + (1 << (4-p_)); 
        });

//    cerr << "[ "; 
//    copy( i.begin(), --i.end(), ostream_iterator<int>(cerr, ", ") );
//    cerr << i.back() << "]" << endl;

    transform( i.begin(), i.end(), p.begin(), i.begin(), 
        []( const int i_, const int p_){
          bitset<4> v(i_);
          return pow( 3, (p_ - v.count())) * pow(2, v.count());
        });

//    cerr << "[ "; 
//    copy( i.begin(), --i.end(), ostream_iterator<int>(cerr, ", ") );
//    cerr << i.back() << "]" << endl;

    transform(n.begin(), n.end(), i.begin(), i.begin(), multiplies<>{});

    return accumulate(i.begin(), i.end(), 0ul ); 
  }

};

istream& operator>>( istream& in, snail_num& sn ) {
  char t;
  int j = 0;
  const auto punc = [&j]( const char c ) {
    switch ( c ) {
      case '[':
        j+=2;
      case ']':
        j-=1;
      case ',':
        return true;
      default:
        return false;
    }
  };

  string s;
  in >> s;
  for_each(s.begin(), s.end(), 
    [&]( auto& t ) -> void 
  {
    if( punc(t) ) return;
    sn.n.push_back( t - '0');
    sn.p.push_back( j );
  });
  return in;
}

ostream& operator<<( ostream& out, const snail_num& sn ) {
  transform(sn.n.begin(), sn.n.end(), sn.p.begin(), ostream_iterator<int>(out), 
    [&out,j = 0]( const int n_, const int p_  ) mutable
    {
      if ( j < p_ )
        out << string( p_ - j, '(' );
      else if ( j > p_ )
        out << string( j - p_, ')' );
      else 
        out << ',';
      j = p_;
      if ( n_ < 10)
        out << ' ';
      return n_;
    });
    return out << string( sn.p.back(), ')' );
}

int main() {

  ifstream in("INPUT");
  vector<snail_num> vsn;
  while ( !in.eof() )
  {
    snail_num sn;
    in >> sn;
    vsn.push_back( sn );
  }
  vsn.pop_back();
//  cerr << vsn.size();

//  cerr << "[ " << endl; 
//  copy_n( vsn.begin(), 2, ostream_iterator<snail_num>(cerr, "\n") );
//  cerr << "... ]" << endl;

//  inclusive_scan( vsn.begin(), vsn.end(), vsn.begin() );

//  cerr << "[ " << endl; 
//  copy_n( vsn.begin(), 2, ostream_iterator<snail_num>(cerr, "\n") );
//  cerr << "..." << endl;
//  cerr << vsn.back() << endl;
//  cerr << " ]" << endl; 

  vector<int> res(vsn.size() * (vsn.size()-1));
  iota( res.begin(), res.end(), 0);
  transform( res.begin(), res.end(), res.begin(),
    [&vsn]( const int i ){ 
      int a = i/vsn.size(), b = i%vsn.size();
      a += (b <= a);
      return (vsn[ a ] + vsn[ b ]).magnitude(); 
    }); 

  cout << *max_element( res.begin(), res.end() ) << endl;
}
