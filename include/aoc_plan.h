#include <tuple>
#include <array>


using dim_t = uint16_t;
using pos_t = tuple<dim_t,dim_t>;

istream& operator>>( istream& in , pos_t& pos ) {
  const auto [x,y] = pos;
  in >> x;
  in.ignore();
  return in >> y;
}

ostream& operator<<( ostream& out , const pos_t& pos ) {
  const auto [x,y] = pos;
  return out << (int)x << "," << (int)y; 
}

// default with tuple
//bool operator<( const pos_t p1, const pos_t p2){
//    const auto [x1,y1] = p1;
//    const auto [x2,y2] = p2;
//    return x1 < x2 || ( x1 == x2 && y1 < y2 );
//};


template<typename T, dim_t S>
  using row_t = array<T,S>;




constexpr dim_t S = 100;

//template<dim_t S>
istream& operator>>(istream& in, row_t<S>& t ) 
{
//  char buf[t.size()];
//  in.get(buf, t.size());
//  transform(buf, buf+t.size(), t.begin(), 
//    [&]( const auto& elm ) { return elm - '0'; });
//
  generate( t.begin(), t.end(), 
    [&](){ 
      char a;
      in >> a;
      return a-'0'; 
    });
  in.ignore();
  return in;
}

//template<dim_t S>
istream& operator>>(istream& in, M<S>& m ) 
{
//  copy_n( istream_iterator<typename M<S>::row_type>( in ), S, m.begin() );
  generate( m.begin(), m.end(), 
    [&](){ 
      M<S>::row_type r;
      in >> r;
      return r; 
    });
  return in;
}

//template<dim_t S>
ostream& operator<<(ostream& out, const array<T,S>& m ) 
{
  copy( m.begin(), m.end(), ostream_iterator<int>( out ) );
  return out << endl;
}

//template<dim_t S>
ostream& operator<<(ostream& out, const M<S>& m ) 
{
//  copy( m.begin(), m.end(), ostream_iterator<array<T,S>>( out, "\n" ) );
//  for_each( m.begin(), m.end(), 
//    [&]( const auto& r ){ 
//      copy( r.begin(), r.end(), ostream_iterator<int>( out ) );
//      out << endl;
//    });
  for( int i = 0; i < 5*S; ++i )
  {
    for( int j = 0; j < 5*S; ++j )
    {
      out << (int) m[{j,i}] << " ";
    }
    out << endl;
  }
  return out;
}


template< dim_t H, dim_t W = H >
set<pos_t> neighbours( const pos_t p )
{
  set<pos_t> rv; 
  const auto [x,y] = p;
  if( x > 0 ) 
    rv.insert( {x-1,y} );
  if( x < W-1)
    rv.insert( {x+1,y} );
  if( y > 0 ) 
    rv.insert( {x,y-1} );
  if( y < H-1)
    rv.insert( {x,y+1} );
  return rv;
}
