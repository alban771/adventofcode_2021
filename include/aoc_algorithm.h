#pragma once

#include <algorithm>


template<class It1, class It2>
It1 find_last_of( It1 first, It1 last, It2 s_first, It2 s_last ) 
{
  while( s_first != s_last && first != last )
  {
    s_last = std::remove_if(s_first, s_last, 
      [&]( const auto& elm ) -> bool
      {
        return *first == elm;
      }),
    ++first;
  }
  return first;
}
