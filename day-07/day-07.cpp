#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <algorithm>
#include <iterator>
#include <numeric>


using namespace std;

int main() {

  fstream in("INPUT");
//  fstream in("EG_1");

  using T = int64_t;

  vector<T> vec;

  while(!in.eof()){
    T t;
    in >> t;
    in.ignore();
    vec.push_back(t);
  }
  vec.pop_back();

  cerr << "[ "; 
  copy_n( vec.rbegin(), 10, ostream_iterator<int>(cerr, ", ") );
  cerr << "... ]" << endl;

  T bi = *min_element(vec.begin(), vec.end());
  T bs = *max_element(vec.begin(), vec.end());

  vector<T> x(bs - bi), d(bs - bi);
  iota(x.begin(), x.end(), bi);

  transform( x.begin(), x.end(), d.begin(), 
      [&](const T elm) {
        return accumulate(vec.begin(), vec.end(), 0ull, 
          [&]( const uint64_t sum, const T elm_ ) 
          {
            T n = abs(elm-elm_);
            return sum +  n * (n+1) / 2;
          });
      });

  cerr << "[ "; 
  copy_n( d.begin(), 10, ostream_iterator<T>(cerr, ", ") );
  cerr << "... ]" << endl;

  cout << *min_element(d.begin(), d.end()) << endl;
}
