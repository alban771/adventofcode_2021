#include <iostream>
#include <fstream>

#include <string>
#include <vector>
#include <set>
#include <map>

#include <algorithm>
#include <utility>
#include <tuple>
#include <numeric>

using namespace std;



//  h-----------------------
using T = tuple<int,int>; 

constexpr auto less_v = []( const T p1, const T p2){
        const auto [x1,y1] = p1;
        const auto [x2,y2] = p2;
        return y1 < y2 || ( y1 == y2 && x1 < x2 );
    };

using M = set<T, decltype(less_v)>;


istream& operator>>(istream& in, T& t ) 
{
  auto& [x,y] = t;
  in >> x;
  in.ignore();
  return in >> y;
}

ostream& operator<<(ostream& out, const M& m )
{
  auto [h,w] = *m.rbegin();
  w = get<0>(*max_element( m.begin(), m.end(), []( const auto p1, const auto p2){
        return get<0>(p1) < get<0>(p2);
      }));

  string s(w+1, '.');
  for_each(m.begin(), m.end(), 
    [&, i=0]( auto& elm ) mutable -> void 
    {
      const auto [x,y] = elm;
      while( i < y )
      {
        ++i;
        out << s << endl; 
        fill(s.begin(), s.end(), '.'); 
      }
      s[x] = '#';
    });

  return out << s << endl;
}


void fold_x( M& m, int x) {
  M _tmp;
  for_each(m.begin(), m.end(),  
    [&]( auto& elm ) -> void 
    {
      if ( get<0>(elm) > x )
        _tmp.emplace( 2*x - get<0>(elm), get<1>( elm ) );
    });
  erase_if( m,
    [&]( auto& elm ) -> bool 
    {
      return get<0>(elm) > x;
    });
  m.merge( _tmp );
}

void fold_y( M& m, int y) {
  M _tmp;
  transform( m.upper_bound({0,y}), m.end(), inserter(_tmp, _tmp.begin()),
    [&]( const auto& elm ) -> T
    {
       return { get<0>(elm), 2*y - get<1>(elm)  };
    });
  m.erase( m.upper_bound({0,y}), m.end() );
  m.merge( _tmp );
}



int main() 
{
//  j----------------------
  fstream in("INPUT");
//  fstream in("EG_1");

  M m;
  vector<T> vec;


  while( in.peek() != '\n' )
  {
    T t;
    in >> t;
    in.ignore();
    const auto [x,y] = t;
    m.emplace( x, y );
  }

  cerr << m << endl;

  in.ignore();
  while( !in.eof() )
  {
    T t;
    auto& [x,y] = t; 
    string fold;
    in >> fold;
    in >> fold;
    in >> fold;
    switch( fold[0] ) 
    {
    case 'x': 
      y=-1;
      x=stoi( fold.substr(2) );
      break;

    case 'y':
      x=-1;
      y=stoi( fold.substr(2) );
    }

    vec.push_back(t);
  }
  vec.pop_back();

//  cerr << get<0>(vec.back()) << "," << get<1>(vec.back()) << endl;

  cerr << m.size() << endl;

  for_each(vec.begin(), vec.end(), 
    [&]( auto& elm ) -> void 
    {
      const auto [x,y] = elm;
      if ( x > -1 )
        fold_x(m, x);
      else
        fold_y(m, y);
    });

  cerr << m << endl;

  cout << m.size()  << endl;
}
