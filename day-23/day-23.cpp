#include <iostream>
#include <fstream>
#include <sstream>

#include <cmath>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <ranges>
#include <numeric>
#include <functional>

#include <array>
#include <vector>
#include <set>
#include <map>
#include <tuple>
#include <stack>

#include <execution>


using namespace std;
namespace rn = ranges;



struct amphipod_t
{
  static constexpr size_t factor[4] = {1,10,100,1000};

  amphipod_t(): type(-1), d(-1) {}
  amphipod_t( const uint8_t t ): type(t){}

  inline operator bool () const { return d != 0xFF; }

  inline size_t energy() const { return d * factor[type]; }

  uint8_t type;
  uint8_t d = 0;
};


template<uint8_t C> struct burrow_t;

template<uint8_t Capacity>
struct room_t: public stack<amphipod_t, vector<amphipod_t>> 
{

  using stack_t = stack<amphipod_t, vector<amphipod_t>>;
  using stack_t::empty;
  using stack_t::size;
  using stack_t::top;

  room_t( const uint8_t t ): stack_t(), type(t) {};

  bool full() const {
    return size() == Capacity && !mixed();
  }

  bool mixed() const {
    return size() > 0 && rn::any_of( c, 
        [&]( const amphipod_t a ){ return a.type != type; });
  }

  bool open() const {
    return size() < Capacity && ( empty() || !mixed() );
  }

  operator size_t() const {
      return accumulate(c.begin(), c.end(), 0ul, 
        []( const size_t sum, const auto& elm ) -> size_t
          { return sum + elm.energy(); });;
  }

  uint8_t type;


  template<uint8_t C> friend 
    ostream& operator<<( ostream&, const burrow_t<C>& );
};


struct hallway_t: public array<amphipod_t, 11>
{
  using array_t = array<amphipod_t, 11>;
  using array_t::size;
  using array_t::begin;
  using array_t::rbegin;
  using array_t::iterator;

  rn::subrange<iterator> free_range( uint8_t d ) {
    using namespace views;
    auto e = rn::find_if( *this | drop(d),  identity() ); 
    d = size()-d-1;
    auto re = rn::find_if( *this | views::reverse | drop(d),  identity() ); 
    d = min(11 - distance(rbegin(),re), distance(begin(), e));
    return rn::subrange( begin() + d, e );
  } 
};


template<uint8_t Capacity>
struct burrow_t 
{
  inline uint8_t addr( uint8_t t ) const { 
    return ++t*2; 
  }

  inline uint8_t dist( uint8_t t , uint8_t i ) const {
    return abs( addr(t) - i );
  }

  inline auto free_range( uint8_t t ) {
    return hallway.free_range( addr(t) );
  }

  burrow_t& push( uint8_t i ){
    h.push_back( i );
    amphipod_t a = hallway[i];
    assert( a );
    assert( rooms[a.type].open() );

    a.d += dist(a.type,i) + Capacity - rooms[a.type].size();
    rooms[a.type].push(a);
    hallway[i] = {};
    return *this;
  }

  burrow_t& pop( uint8_t t, uint8_t i ){
    h.push_back( (t+1 << 4) + i );
    uint8_t d = dist(t,i);
    assert( !hallway[i] ); 
    assert( !rooms[t].empty() ); 
    assert( d != 0 );

    amphipod_t a = rooms[t].top();
    rooms[t].pop();
    a.d += d + Capacity - rooms[t].size();
    hallway[i] = a;
    return *this;
  }

  burrow_t cpy_push( uint8_t i ) const {
    burrow_t cpy = *this;
    return cpy.push( i );
  }

  burrow_t cpy_pop( uint8_t t, uint8_t i ) const {
    burrow_t cpy = *this;
    return cpy.pop( t, i );
  }

  operator size_t() const {
    return h.size() == 0 ? -1ul : 
        accumulate( rooms.begin(), rooms.end(), 0ul ); 
  }

  hallway_t hallway;
  vector<room_t<Capacity>> rooms = {0, 1, 2, 3};
  vector<uint8_t> h;
};


ostream& operator<<(ostream& out, const amphipod_t& a ) {
  if (a)
    out << (char)(a.type + 'A');
  else
    out << '.';
  return out;
}


template<uint8_t C>
ostream& operator<<(ostream& out, const burrow_t<C>& b ) {
  cerr << string( 13, '#' ) << endl; 
  cerr << '#';
  rn::copy( b.hallway, ostream_iterator<amphipod_t>(cerr) );
  cerr << '#' << endl << "##";
  for( int i = 0; i < C; ++i )
  {
    for ( room_t r : b.rooms )
      cerr << "#" << (char)(r.size() >= C - i ? r.c[C-i-1].type + 'A': '.');
    if ( i == 0 )
      cerr << "##";
    cerr << "#\n  ";
  }
  return cerr << "#########" << endl;
} 


template<uint8_t C>
burrow_t<C> sort( burrow_t<C>&& b ) 
{
  bool stic;
  do {
    stic = true;
    for( room_t<C>& r : b.rooms | views::filter( 
          []( const room_t<C>& r ){ return r.open(); })) {
      auto [first,last] = b.free_range( r.type );
      size_t d = distance( b.hallway.begin(), first );
      if ( last != b.hallway.end() && last->type == r.type )
      {
        stic = false;
        b.push( d + (last - first) );
      }
      if ( last != first && first != b.hallway.begin() && (--first)->type == r.type)
      {
        stic = false;
        b.push( d - 1 );
      }
    }

    for( room_t<C>& r : b.rooms | views::filter( 
          [&b]( const room_t<C>& r ){ 
            return r.mixed() &&
                   r.top().type != r.type &&
                   b.rooms[r.top().type].open(); 
          })) {
      const auto free = b.free_range( r.type );
      uint8_t d = distance( b.hallway.begin(), free.begin() );
      uint8_t ad = b.addr( r.top().type );
      if ( ad > d && ad < d + free.size() )
      {
        stic = false;
        b.pop( r.type, ad );
        b.push( ad );
      }
    }

  }
  while( !stic );


  if( rn::all_of( b.rooms, [](const room_t<C>& r){ return r.full(); }))
    return b;

  vector<uint8_t> filtered;

  rn::transform( b.rooms | views::filter([&b](const room_t<C>& r){ 
            return r.mixed() && b.free_range( r.type ).size() > 1; 
          }), back_inserter(filtered), 
      [](const room_t<C>& r){ return r.type; });

  if ( filtered.size() == 0 ) 
    return burrow_t<C>{};

  const auto recurse = [&]( const uint8_t rt ) -> burrow_t<C> 
    {
      auto free = b.free_range( rt );
      size_t d = distance( b.hallway.begin(), free.begin() );
      vector<burrow_t<C>> rv_( free.size() - 1 );
      rn::transform( views::iota( d, d + free.size() ) | views::filter(
            []( const int i ){ return i % 2 == 1 || i == 0 || i == 10; }), rv_.begin(), 
        [&]( const int i )
        {
          return sort( b.cpy_pop( rt, i ) );
        });
        return *rn::min_element( rv_ ); 
    };

  if ( filtered.size() == 1 )
    return recurse( filtered[0] );

  vector<burrow_t<C>> rv( filtered.size() );
  transform( execution::par, filtered.begin(), filtered.end(), rv.begin(), recurse );
  return *rn::min_element( rv );
}




int main() {

  constexpr size_t S = 4;
  ifstream in("EG1");
  burrow_t<S> b;

  string skip;


  in >> skip >> skip;
  in.read( skip.data(), 3 );
  string row[S];
  for( int i = 0; i < S; ++i )
    in >> row[i];

  for( int i = 0; i < S; ++i )
  {
    b.rooms[0].emplace( row[S-i-1][1] - 'A');
    b.rooms[1].emplace( row[S-i-1][3] - 'A');
    b.rooms[2].emplace( row[S-i-1][5] - 'A');
    b.rooms[3].emplace( row[S-i-1][7] - 'A');
  }


  cerr << b << endl;
  burrow_t<S> s = sort( burrow_t<S>{b} );

  for ( uint8_t h : s.h ) 
  {
    if ( ( h & 0x70 ) == 0 )
      b.push( h );
    else
      b.pop( (h >> 4) - 1, h & 0xF );
    cerr << b << endl;
  }

  cout << (size_t)s << endl;


}
