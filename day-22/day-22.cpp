#include <iostream>
#include <fstream>
#include <sstream>

#include <cmath>
#include <cassert>

#include <vector>
#include <string>
#include <bitset>
#include <set>
#include <map>
#include <tuple>

#include <iterator>
#include <algorithm>
#include <numeric>



using namespace std;

struct step;
struct cube;

ostream& operator<<( ostream& out, const step& t );


using point_t = tuple<int,int,int>;

point_t operator+( const point_t lhs, const point_t rhs ){
  return { 
    get<0>(lhs) + get<0>(rhs),
    get<1>(lhs) + get<1>(rhs),
    get<2>(lhs) + get<2>(rhs),
  };
}

ostream& operator<<( ostream& out, const point_t& t );

struct cube {

  cube( const point_t& a, const point_t& g ): A(a), G(g) {}

  template<int s1 = -1, int s2 = -1, int s3 = -1>
  cube& swap_elm() {
    if constexpr ( s1 >= 0 )
      swap(get<s1>(A), get<s1>(G));
    if constexpr ( s2 >= 0 )
      swap(get<s2>(A), get<s2>(G));
    if constexpr ( s3 >= 0 )
      swap(get<s3>(A), get<s3>(G));
    return *this;
  }


  point_t A, G;

  // inter
  cube operator/( const cube& oth ) const {
    const auto [x0, y0, z0] = A;
    const auto [x1, y1, z1] = G;

    const auto [x0__, y0__, z0__] = oth.A;
    const auto [x1__, y1__, z1__] = oth.G;

    int x0_ = max(x0, x0__);
    int y0_ = max(y0, y0__);
    int z0_ = max(z0, z0__);
    int x1_ = min(x1, x1__);
    int y1_ = min(y1, y1__);
    int z1_ = min(z1, z1__);

    return { {x0_, y0_, z0_}, {x1_, y1_, z1_} };
  }

  operator bool() const {
    const auto [x0, y0, z0] = A;
    const auto [x1, y1, z1] = G;
    return x0 <= x1 && y0 <= y1 && z0 <= z1;
  }

  vector<point_t> sommets() const {
    const auto [x0, y0, z0] = A;
    const auto [x1, y1, z1] = G;
    return {
      {x0,y0,z0}, // A
      {x0,y1,z0}, // B
      {x1,y1,z0}, // C
      {x1,y0,z0}, // D
      {x0,y0,z1}, // E
      {x0,y1,z1}, // F
      {x1,y1,z1}, // G
      {x1,y0,z1}  // H
    };
  }


  constexpr static point_t offset[8] = {
      {0,0,-1},
      {0,1,0 },
      {1,0,0 },
      {0,0,1 },
      {0,-1,0},
      {-1,0,0},
      {1,-1,-1},
      {-1,1,1}
  };

  vector<cube> operator-( const cube& oth ) const {

    cube o = oth / *this;
    if ( !o ) return { *this };
    
    vector<point_t> t_smts = sommets();
    vector<point_t> o_smts = o.sommets();

    vector<cube> rv = { 
        cube{t_smts[0], o_smts[2] + offset[0]}.swap_elm<>(),     // A C__
        cube{t_smts[1], o_smts[6] + offset[1]}.swap_elm<1>(),     // B G__
        cube{t_smts[2], o_smts[7] + offset[2]}.swap_elm<0,1>(),     // C H__
        cube{t_smts[6], o_smts[4] + offset[3]}.swap_elm<0,1,2>(),     // G E__
        cube{t_smts[7], o_smts[0] + offset[4]}.swap_elm<0,2>(),     // H A__
        cube{t_smts[4], o_smts[1] + offset[5]}.swap_elm<2>(),     // E B__
        cube{t_smts[3], o_smts[3] + offset[6]}.swap_elm<0>(),     // D D__
        cube{t_smts[5], o_smts[5] + offset[7]}.swap_elm<1,2>()      // F F__
    };

    rv.erase( 
      remove_if(rv.begin(), rv.end(), 
        [&]( const auto& elm ) -> bool { return !elm; }),
      rv.end());
    return rv;
  }


  uint64_t size() const {
    const auto [x0, y0, z0] = A;
    const auto [x1, y1, z1] = G;
    uint64_t l1 = x1 - x0 + 1ull, l2 = y1 - y0 + 1ull, l3 = z1 - z0 + 1ull;
    return l1 * l2 * l3;
  }

};




struct step {

  bool power;
  int x0,x1,y0,y1,z0,z1;

  operator cube() {
    return { {x0,y0,z0}, {x1,y1,z1} };
  }

  operator bool() const {
    return true;
//    vector<int> val = {x0,x1,y0,y1,z0,z1};
//    return !any_of( val.begin(), val.end(), [](const int i){ 
//        return abs(i) > 50;
//        }); 
  }
};





istream& operator>>( istream& in, step& t ) 
{
  string s;
  in >> s;
  t.power = s == "on";
  in >> s;
  in.ignore();
  stringstream is(s);
  char c;
  is >> c >> c >> t.x0;
  is >> c >> c >> t.x1;
  is >> c >> c >> c >> t.y0;
  is >> c >> c >> t.y1;
  is >> c >> c >> c >> t.z0;
  is >> c >> c >> t.z1;
  return in;
}


ostream& operator<<( ostream& out, const step& t ) 
{
  out << (t.power ? "on" : "off");
  out << " X: " << t.x0 << ", " << t.x1 << "\t";
  out << " Y: " << t.y0 << ", " << t.y1 << "\t";
  out << " Z: " << t.z0 << ", " << t.z1 << "\t";
  return out;
}


ostream& operator<<( ostream& out, const point_t& p ) 
{
  out << " X:" << get<0>(p) << ", "; 
  out << " Y:" << get<1>(p) << ", ";
  out << " Z:" << get<2>(p);
  return out;
}

ostream& operator<<( ostream& out, const cube& c ) 
{
  return out << c.A << "\t///\t" << c.G;
}




int main() {


  ifstream in("INPUT");
//  ifstream in("EG2");

  vector<step> inpt;

  while( !in.eof() )
  {
    step t;
    in >> t;
    inpt.push_back( t );
  }
  inpt.pop_back();

  cout << "inpt #" << inpt.size() << " [ " << endl; 
  copy_n( inpt.rbegin(), min(10ul, inpt.size()), ostream_iterator<step>(cout, "\n"));
  cout << "... ]" << endl;


  inpt.erase( 
    remove_if(inpt.begin(), inpt.end(), 
      [&]( const auto& elm ) -> bool
      {
        return !elm;
      }),
    inpt.end());

  vector<cube> res = {};
  for ( step s : inpt ) 
  {
    vector<cube> next; 

    if ( s.power )
    {
      next.push_back( s );
      for ( cube c1 : res ) {
        vector<cube> op_next = {};
        for ( cube c2 : next ) {
          vector<cube> op_ = c2 - c1;
          copy( op_.begin(), op_.end(), back_inserter(op_next) );
        }
        next = move( op_next );
      }
      copy( next.begin(), next.end(), back_inserter(res));
    }
    else
    {
      for ( cube c : res ) 
      {
        vector<cube> op_ = c - s; 
        copy( op_.begin(), op_.end(), back_inserter(next));
      }
      res = move( next );
    }

    cerr << "res #" << res.size() << " [" << endl; 
//    copy( res.begin(), res.end(), ostream_iterator<cube>(cerr, "\n") );
//    cerr << "]" << endl;
  }


  cout << accumulate(res.begin(), res.end(), 0ull, 
    [&]( const uint64_t sum, const auto& elm ) -> size_t
    {
      return sum + elm.size();
    }) << endl;

}
