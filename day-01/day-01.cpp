#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>

using namespace std;

template<typename T, T threshold>
struct greater_to {
  static constexpr T value = threshold;
  bool operator()( const T e ){ return e > threshold; };
};

template<typename T = int>
  using positiv = greater_to<T, 0>;

int main() {

  ifstream in("INPUT");
  vector<int> v(2000), w, x, z;
  copy_n( istream_iterator<int>(in), v.size(), v.begin() );


  adjacent_difference( v.begin(), v.end(), back_inserter(w) );
  w.erase(w.begin());
  cout << count_if(w.begin(), w.end(), positiv() ) << endl;

  transform( w.begin() + 2, w.end(), back_inserter(x), 
      [it=--w.begin()] (auto e) mutable {
      ++it;
      return accumulate(it, it + 2, e);
      });

  cout << count_if(x.begin(), x.end(), positiv() ) << endl;
}



