#include <iostream>
#include <fstream>

#include <string>
#include <vector>
#include <set>
#include <map>

#include <algorithm>
#include <utility>
#include <tuple>
#include <numeric>

using namespace std;


//  h-----------------------
using T = tuple<char,char>; 

constexpr auto less_v = []( const T p1, const T p2){
        const auto [x1,y1] = p1;
        const auto [x2,y2] = p2;
        return x1 < x2 || ( x1 == x2 && y1 < y2 );
    };

using M = map<T, char>;

istream& operator>>(istream& in, T& t ) 
{
  string s;
  auto& [x,y] = t;
  in >> x;
  in >> y;
  return in >> s;
}

ostream& operator<<(ostream& out, const T& t ) 
{
  auto& [x,y] = t;
  return out << x << y << " -> ";
}

//ostream& operator<<(ostream& out, const M& m )
//{
//  auto [h,w] = *m.rbegin();
//  w = get<0>(*max_element( m.begin(), m.end(), []( const auto p1, const auto p2){
//        return get<0>(p1) < get<0>(p2);
//      }));
//
//  string s(w+1, '.');
//  for_each(m.begin(), m.end(), 
//    [&, i=0]( auto& elm ) mutable -> void 
//    {
//      const auto [x,y] = elm;
//      while( i < y )
//      {
//        ++i;
//        out << s << endl; 
//        fill(s.begin(), s.end(), '.'); 
//      }
//      s[x] = '#';
//    });
//
//  return out << s << endl;
//}


//void fold_x( M& m, int x) {
//  M _tmp;
//  for_each(m.begin(), m.end(),  
//    [&]( auto& elm ) -> void 
//    {
//      if ( get<0>(elm) > x )
//        _tmp.emplace( 2*x - get<0>(elm), get<1>( elm ) );
//    });
//  erase_if( m,
//    [&]( auto& elm ) -> bool 
//    {
//      return get<0>(elm) > x;
//    });
//  m.merge( _tmp );
//}
//
//void fold_y( M& m, int y) {
//  M _tmp;
//  transform( m.upper_bound({0,y}), m.end(), inserter(_tmp, _tmp.begin()),
//    [&]( const auto& elm ) -> T
//    {
//       return { get<0>(elm), 2*y - get<1>(elm)  };
//    });
//  m.erase( m.upper_bound({0,y}), m.end() );
//  m.merge( _tmp );
//}

//  j----------------------


int main() 
{
  fstream in("INPUT");
//  fstream in("EG_1");
//  k----------------------

  char last;
  map<T,size_t> s;
  M m;

  {
    string _s;
    in >> _s;
    in.ignore();
    in.ignore();
    vector<T> _t(_s.size() - 1);
    transform(_s.begin(), --_s.end(), ++_s.begin(), _t.begin(),
      [&]( const char c1, const char c2 )
      {
        return T{c1, c2};
      });
    for_each(_t.begin(), _t.end(), 
      [&]( const auto& t ) -> void 
      {
        if ( s.find(t) == s.end() )
          s[t] = 0;
        ++s[t];
        cerr << t << s[t] << endl;
      });
    last = _s.back();
  }

  cerr << m.size() << endl;

  while( in.peek() != '\n' && in.peek() != '\0' && !in.eof() )
  {
    T t;
    char c;
    in >> t;
    in >> c;
    in.ignore();
    m.emplace( t, c );
  }

  cerr << m.size() << endl;


  for( int i = 0; i < 40; ++i )
  {
//    string s_(s);
//    auto it = s.begin();
//    int j = 0;
//    while ( it = adjacent_find(it, s.end(), 
//        [&]( auto& e1, auto& e2 ) -> bool 
//        {
//          return m.find(T{e1,e2}) != m.end();
//        }), it != s.end() )
//    {
//      s_.insert( it - s.begin() + ++j,1, m[ T{ *it, *(it+1)}] );
//      ++it;
//    };

    map<T,size_t> s_(s);
    for_each(s.begin(), s.end(), 
      [&]( auto& elm ) -> void 
      {
        const auto [t,x] = elm;
        auto it = m.find(t);
        if ( it != m.end() )
        {
          const auto [_,c] = *it;
          const auto [a,b] = t;
          s_[t] -= x;
          s_[T{a,c}] += x;
          s_[T{c,b}] += x;
        }
      });

    s = move(s_);
//    cerr << i << endl;
  }

  vector<char> a(26);
  vector<size_t> l(26);
  iota( a.begin(), a.end(), 'A');
  transform(a.begin(), a.end(), l.begin(), 
    [&]( const auto c )
    {
      return accumulate(s.lower_bound( T{c, 'A'}), s.upper_bound( T{c,'Z'} ), 0ll, 
        [&]( const size_t sum, const auto& elm ) -> size_t
        {
          return sum + get<1>(elm);
        });;
    });

  l[ last - 'A' ] = accumulate(s.begin(), s.end(), 0ll, 
        [&]( const size_t sum, const auto& elm ) -> size_t
        {
          const auto [t,x] = elm;
          return sum + ( get<1>(t) == last ? x : 0 );
        });; 

  auto le = remove_if( l.begin(), l.end(), []( const auto elm ){ return elm == 0;} );

  const auto [mn,mx] = minmax_element( l.begin(), le );

  cout << *mx - *mn << endl;
}
