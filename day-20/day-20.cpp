#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <bitset>
#include <tuple>
#include <string>
//#include <ranges>

//#include "aoc_plan.h"

using namespace std;
//using namespace ranges as rn;


int main() {


  ifstream in("INPUT");
  string ccode;
  in >> ccode;

  int margin = 50;
  in.ignore();
  vector<string> frame(margin, "");
  for(string row; in >> row, !in.eof(); )
  {
    row.append(string(margin*2, '.'));
    rotate( row.begin(), row.end()-margin, row.end());
    frame.push_back(row);
  }

  int W = frame.back().size();
  int H = frame.size() + margin;

  for( int i = 0; i < margin; ++i )
  {
    frame.push_back(string(W, '.'));
    frame[i] = frame.back();
  }

//  copy( frame.begin(), frame.end(), ostream_iterator<string>(cout, "\n") );
//  cout << endl;

  for( int _ = 0; _ < margin; ++_ )
  {
    vector<string> f_(H);
    transform(frame.begin(), frame.end(), f_.begin(),
      [&,i=0]( const string& s ) mutable 
      {
        string s_(W, ' ');
        transform(s.begin(), s.end(), s_.begin(), 
          [&,j=0]( const char c ) mutable
          {
            string mask(9, frame[0][0]);
            for( int i_ = max(0,i-1); i_ < min(H,i+2) ; ++i_ )
            {
              copy( frame[i_].begin()+max(0,j-1), frame[i_].begin()+min(W,j+2), 
                  mask.begin() + (i_-i+1)*3 + (j == 0) );
            }

//            if ( i == 1 )
//              cout << "m: " << mask << endl;
            bitset<9> b(mask,0, 9, '.', '#');
            ++j;
            return ccode[b.to_ulong()];
          });;
        ++i;
        return s_;
      });

    frame = move(f_);
  }

  copy( frame.begin(), frame.end(), ostream_iterator<string>(cout, "\n") );
  cout << endl;

  cout << accumulate(frame.begin(), frame.end(), 0, 
    [&]( const size_t sum, const auto& elm ) -> size_t
    {
      return sum + count(elm.begin(), elm.end(), '#');
    });
}
