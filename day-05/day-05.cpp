#include <iostream>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <fstream>
#include <sstream>

#include <vector>
#include <string>
#include <array>
#include <set>
#include <unordered_set>

#include <cassert>

#include "aoc_algorithm.h" 

using namespace std;


template<size_t N>
struct pt
{
  int x,y;
  bool operator<( const pt oth ) const {
    return (x + y*N) < (oth.x + oth.y*N);
  }
};


template<size_t N>
ostream& operator<<(ostream& out, const pt<N>& p) {
  return out << "(" << p.x << "," << p.y << ")"; 
}

struct comp 
{
//  g----------------------------------------------------------
  comp(): x1(0), x2(0), y1(0), y2(0) {}
  comp( int x, int y ): x1(x), x2(x), y1(y), y2(y) {}
  comp( int x1, int y1, int x2, int y2 ): x1(x1), x2(x2), y1(y1), y2(y2) {}
  int x1, y1, x2, y2;
  int d() const { 
    if( x1 != x2 )
      return (y2 - y1) / (x2 - x1);
    else
      return 8;
  };

  size_t size() const { return abs( y2 - y1 ) + abs( x2 - x1 ) + 1; }
  void flip() 
  {
    swap(x1, x2);
    swap(y1, y2);
  }

  int operator[]( int x ) const {
    assert( x1 != x2 );
    return y1 + ( x - x1 ) * d();
  }
};


istream& operator>>( istream& in, comp& c )
{
  string f;
  in >> c.x1;
  in.ignore();
  in >> c.y1;
  in >> f;
  in >> c.x2;
  in.ignore();
  in >> c.y2; 
  return in.ignore();
}



ostream& operator<<( ostream& out, const comp& c )
{
  return out << c.x1 << "," << c.y1 << " - "  << c.x2 << "," << c.y2;
}



template<int d = 0>
const auto overlap = []( comp c1 , comp c2 ) -> comp
{
  assert( c1.d() == d );
  assert( c2.d() == d );

  if ( c1[0] != c2[0] ) { 
    cerr << "err";
    return {};
  }

  int mx1 = max( c1.x1, c2.x1 ); 
  int mx2 = min( c1.x2, c2.x2 ); 
  if ( mx2 < mx1 ) return {};
  return { mx1, c1[mx1], mx2, c1[mx2] };
};



template<>
const auto overlap<8> = []( comp c1 , comp c2 ) -> comp {
  assert( c1.x1 - c1.x2 == 0 );
  assert( c2.x1 - c2.x2 == 0 );
    
  if ( c1.x1 != c2.x1 ) return {};

  int my1 = max( c1.y1, c2.y1 ); 
  int my2 = min( c1.y2, c2.y2 ); 
  if ( my2 < my1 ) return {};
  return { c1.x1, my1, c1.x1, my2 };
};


template<size_t N>
const pt<N> inter( const comp rhs, const comp lhs )
{

  if ( lhs.d() == 8 )
    return inter<N>( lhs, rhs );

  if( rhs.d() == 8 ) 
  {
    int x = rhs.x1;
    if ( lhs.x1 > x || lhs.x2 < x )
    {
//      cerr << "out of X domain" << endl;
      return {};
    }
    int y = lhs[x];
    if ( rhs.y1 > y || rhs.y2 < y )
    {
//      cerr << "out of Y domain" << endl;
      return {};
    }

//    cerr << "ret: " << pt<N>{ x, y } << endl;
    return { x, y };
  }

  int d = abs( rhs.d() - lhs.d());
  int dy = abs( lhs[rhs.x1] - rhs.y1 );
  if ( d == 2 && dy % 2 == 1 ) 
  {
//    cerr << "cross" << endl;
    return {};
  }
  int x = rhs.x1 + (dy / d);
  if ( x > rhs.x2 || lhs.x1 > x || lhs.x2 < x )
  {
//    cerr << "out of domain" << endl;
    return {};
  }

  int y = rhs[x];
  if ( y != lhs[x] )
  {
//    cerr << "bad value G: " << y << " D: " << lhs[x] << endl;
    return {};
  }

//  cerr << "ret: " << pt<N>{ x, y } << endl;
  return { x, y };
};



int main() 
{
  ifstream in("INPUT");  
  const size_t N = 1000;
//  const size_t M = 10;

//  ifstream in("EG_1");  
//  const size_t N = 10;
//  const size_t M = 10;

//  ifstream in("EG_2");  
//  const size_t N = 5;
//  const size_t M = 10;

  using T = comp;
  
  vector<T> V;


  while ( !in.eof() ) 
  {
//  h----------------------------------------------------------
    T t;
    in >> t;
    V.push_back(t);
  }
  V.pop_back();


//  cerr << "[ "; 
//  copy( V.begin(), --V.end(), ostream_iterator<comp>(cerr, "\n") );
//  cerr << V.back() << "]" << endl;


//  j----------------------------------------------------------


  auto b = V.begin();

  auto v = partition(b, V.end(), 
    [&]( const auto& elm ) -> bool
    {
      return elm.x1 == elm.x2 || elm.y1 == elm.y2;
    });
  auto h = partition(b, v, 
    [&]( const auto& elm ) -> bool
    {
      return elm.y1 == elm.y2;
    });
  auto f = partition(v, V.end(), 
    [&]( const auto& elm ) -> bool
    {
      return elm.d() == 1;
    });
  auto l = V.end();

  cerr << "h #" << h - V.begin() << endl;
  cerr << "v #" << v - h << endl;
  cerr << "f #" << f - v << endl;
  cerr << "l #" << V.end() - f << endl;

  for_each(V.begin(), V.end(), 
    [&]( auto& elm ) -> void 
    {
      if ( elm.x2 < elm.x1 ) elm.flip();
    });
  for_each(h, v, 
    [&]( auto& elm ) -> void 
    {
      if ( elm.y2 < elm.y1 ) elm.flip();
    });

  sort( V.begin(), h, []( const auto e1, const auto e2){
        return e1.y1 < e2.y1 || ( e1.y1 == e2.y1 && e1.x1 < e2.x1 );
      });
  sort( h, v, []( const auto e1, const auto e2){
        return e1.x1 < e2.x1 || ( e1.x1 == e2.x1 && e1.y1 < e2.y1);
      });
  sort( v, f, []( const auto e1, const auto e2){
        int o1 =  e1.x1 - e1.y1, o2 = e2.x1 - e2.y1;
        return o1 < o2 || ( o1 == o2 && e1.x1 < e2.x1 );
      });
  sort( f, l, []( const auto e1, const auto e2){
        int o1 =  e1.x1 + e1.y1, o2 = e2.x1 + e2.y1;
        return o1 < o2 || ( o1 == o2 && e1.x1 < e2.x1 );
      });

  cerr << "V: [ "; 
  copy_n( V.begin(), min((size_t)10, V.size()), ostream_iterator<T>(cerr, "\n") );
  cerr << "... ]" << endl;

  set<pt<N>> S;

  auto it = V.begin();
  while ( it = adjacent_find( it, h, [](const auto e1, const auto e2){
              return e1.y1 == e2.y1;
          }), it != h ) 
  {
    T o_ = overlap<0>( *it, *(it++) );
    for( int i = o_.x1; i <= o_.x2; ++i )
      S.emplace( i, o_.y1 );
  }
  while ( it = adjacent_find( it, v, [](const auto e1, const auto e2){
              return e1.x1 == e2.x1;
          }), it != v ) 
  {
    T o_ = overlap<8>( *it, *(it++) );
    for( int i = o_.y1; i <= o_.y2; ++i )
      S.emplace( o_.x1, i );
  }
  while ( it = adjacent_find( it, f, [](const auto e1, const auto e2){
              return e1.x1 - e1.y1 == e2.x1 - e2.y1;
          }), it != f ) 
  {
    T o_ = overlap<1>( *it, *(it++) );
    for( int i = o_.x1; i <= o_.x2; ++i )
      S.insert( { i, o_[i] } );
  }
  while ( it = adjacent_find( it, l, [](const auto e1, const auto e2){
              return e1.x1 + e1.y1 == e2.x1 + e2.y1;
          }), it != l ) 
  {
    T o_ = overlap<-1>( *it, *(it++) );
    for( int i = o_.x1; i <= o_.x2; ++i )
      S.insert( { i, o_[i] } );
  }


//  k----------------------------------------------------------



  for_each( b, h, 
    [&]( auto& elm ) -> void 
    {
      for_each(h, l, 
        [&]( auto& elm_ ) -> void 
        {
          S.insert( inter<N>(elm, elm_) );
        });
    });

  for_each( h, v, 
    [&]( auto& elm ) -> void 
    {
      for_each(v, l, 
        [&]( auto& elm_ ) -> void 
        {
          S.insert( inter<N>(elm, elm_) );
        });
    });

  for_each( v, f, 
    [&]( auto& elm ) -> void 
    {
      for_each(f, l, 
        [&]( auto& elm_ ) -> void 
        {
          S.insert( inter<N>(elm, elm_) );
        });
    });


//  cerr << "S: [ "; 
//  copy( S.begin(), --S.end(), ostream_iterator<pt<N>>(cerr, "\n") );
//  cerr << *--S.end() << " ]" << endl;

  cout << S.size() - 1 << endl;
}
