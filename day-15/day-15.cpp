#include <iostream>
#include <fstream>

#include <array>
#include <vector>
#include <set>
#include <map>

#include <algorithm>
#include <utility>
#include <iterator>
#include <tuple>
#include <numeric>

using namespace std;


//  h-----------------------
using dim_t = uint16_t;
using T = unsigned short;
using pos_t = tuple<dim_t,dim_t>;

ostream& operator<<( ostream& out , const pos_t& pos ) {
  const auto [x,y] = pos;
  return out << (int)x << "," << (int)y; 
}

template<dim_t S>
  using row_t = array<T,S>;

template<dim_t S, typename r = row_t<S>>
struct M : public array<r,S>
{
  using row_type = r;

  const T operator[]( const pos_t p ) const {
    const auto [x,y] = p;
    dim_t a = x / S;
    a += y / S;
    return 1+ (( array<r,S>::operator[](y % S)[x % S] + a - 1 ) % 9);
  }
};

constexpr auto less_v = []( const pos_t p1, const pos_t p2){
        const auto [x1,y1] = p1;
        const auto [x2,y2] = p2;
        return x1 < x2 || ( x1 == x2 && y1 < y2 );
    };

constexpr dim_t S = 100;

//template<dim_t S>
istream& operator>>(istream& in, row_t<S>& t ) 
{
//  char buf[t.size()];
//  in.get(buf, t.size());
//  transform(buf, buf+t.size(), t.begin(), 
//    [&]( const auto& elm ) { return elm - '0'; });
//
  generate( t.begin(), t.end(), 
    [&](){ 
      char a;
      in >> a;
      return a-'0'; 
    });
  in.ignore();
  return in;
}

//template<dim_t S>
istream& operator>>(istream& in, M<S>& m ) 
{
//  copy_n( istream_iterator<typename M<S>::row_type>( in ), S, m.begin() );
  generate( m.begin(), m.end(), 
    [&](){ 
      M<S>::row_type r;
      in >> r;
      return r; 
    });
  return in;
}

//template<dim_t S>
ostream& operator<<(ostream& out, const array<T,S>& m ) 
{
  copy( m.begin(), m.end(), ostream_iterator<int>( out ) );
  return out << endl;
}

//template<dim_t S>
ostream& operator<<(ostream& out, const M<S>& m ) 
{
//  copy( m.begin(), m.end(), ostream_iterator<array<T,S>>( out, "\n" ) );
//  for_each( m.begin(), m.end(), 
//    [&]( const auto& r ){ 
//      copy( r.begin(), r.end(), ostream_iterator<int>( out ) );
//      out << endl;
//    });
  for( int i = 0; i < 5*S; ++i )
  {
    for( int j = 0; j < 5*S; ++j )
    {
      out << (int) m[{j,i}] << " ";
    }
    out << endl;
  }
  return out;
}


template< dim_t H, dim_t W = H >
set<pos_t> neighbours( const pos_t p )
{
  set<pos_t> rv; 
  const auto [x,y] = p;
  if( x > 0 ) 
    rv.insert( {x-1,y} );
  if( x < W-1)
    rv.insert( {x+1,y} );
  if( y > 0 ) 
    rv.insert( {x,y-1} );
  if( y < H-1)
    rv.insert( {x,y+1} );
  return rv;
}


//  j----------------------


using path_t = tuple<T, pos_t>;

bool operator==( const path_t& lhs, const path_t& rhs ) 
{
  return get<1>(lhs) == get<1>(rhs);
}


template<typename Map>
T path_val( const path_t& p, const Map& m ) {
  const auto [v,pos] = p;
  return v + m[pos];
}


template<typename Map>
constexpr auto less_path( const Map& m ) {  
  return [&]( const path_t& p1, const path_t& p2 )
    {
      return path_val(p1, m) < path_val(p2, m);
    };
}






int main() 
{
  ifstream in("INPUT");
//  ifstream in("EG_1");
  using path_t = path_t;

//  k----------------------

  M<S> m;
  in >> m;
//  copy_n( istream_iterator<typename M<S>::row_type>( in ), S, m.begin() );
//  cerr << m << endl;


   
  set<pos_t> visited = {};
  multiset<path_t, decltype(less_path(m))> pool{ {{0,{0,0}}}, less_path(m)};
//  multiset<path_t> pool = {{{0,0}}};
  path_t p = {0,{0,0}};
  const pos_t e = { 5*S-1, 5*S-1};
  dim_t d = 0;

  while ( get<1>(p) != e )
  {

//    const auto [b,e] = pool.equal_range( *pool.begin() );



    // want a move here
    p = move(*pool.begin()); 

    auto it = pool.begin();
    while( it != pool.end() )
    {
      pool.erase( it );
      it = find( pool.begin(), pool.end(), p);
    }



    visited.insert( get<1>(p) );

//    const auto [x,y] = get<1>(p);
//    auto lb = visited.lower_bound( {0,y} );
//    auto ub = visited.upper_bound( {x,y} );
//    dim_t a = get<0>(*lb);
//    if ( distance(lb, ub) == x - a && x - a > 2 )
//    {
//      bool e = true;
//      int d = x - a - 2;
//      if ( y != 0 ) 
//      {
//        const auto lb1 = visited.lower_bound( {0,y-1} );
//        e &= distance(lb1, lb) > 0;
//        const dim_t x1 = get<0>(*prev(lb));
//        const dim_t a1 = get<0>(*lb1);
//        e &= ( distance(lb1, lb) == x1 - a1);
//        e &= x1 > a;
//        d = min( d, x1 - a );
//      }
//      if ( y != 5*S-1 )
//      {
//        const auto ub1 = visited.upper_bound( {x,y+1} );
//        e &= distance(ub, ub1) > 0;
//        const dim_t x1 = get<0>(*prev(ub1));
//        const dim_t a1 = get<0>(*ub);
//        e &= ( distance( ub, ub1 ) == x1 - a1);
//        e &= x1 > a;
//        d = min( d, x1 - a );
//      }
//      if ( e )
//        visited.erase(lb, next(lb,d));
//    }


    set<pos_t> n = neighbours<5*S>( get<1>(p) );
    vector<pos_t> _n;
    // uses that ranges are sorted
    set_difference( n.begin(), n.end(), 
        visited.lower_bound(*n.begin()), visited.upper_bound(*--n.end()), 
        back_inserter(_n));
    transform( _n.begin(), _n.end(), inserter(pool, pool.begin()), 
        [v=path_val(p, m)]( const pos_t pos ) -> path_t { 
          return {v, pos}; 
        });



  }


  cerr << pool.size() << endl;


//  copy( p.begin(), p.end(), ostream_iterator<pos_t>(cerr, " ") );
//  for_each(p.begin(), p.end(), 
//    [&]( auto& elm ) -> void 
//    {
//      cerr << elm;
//      cerr << " ";
//    });
//  cerr << endl;

//  auto it = find_if( pool.begin(), pool.end(), 
//        []( const path_t& p ){ 
//          return p.back() == pos_t{S-1, S-1};
//        });

//  if ( it != pool.end() )
  // TODO: use ranges
  cout << ( path_val( p, m ) - m[{0,0}] ) << endl;
}
