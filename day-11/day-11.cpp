#include <iostream>
#include <fstream>
#include <array>
#include <set>
#include <utility>
#include <iterator>


using namespace std;


using dim_t = uint8_t;
using pos_t = pair<dim_t,dim_t>;


pos_t operator+( const pos_t p1, const pos_t p2 )
{
  return { p1.first + p2.first, p1.second + p2.second };
}

template<dim_t H, dim_t W>
set<pos_t> neighbours( const pos_t p )
{
  set<pos_t> rv; 
  const auto [r,im] = p;
  for( dim_t i = max( r-1, 0 ); i < min( r+2, W+0 ); ++i )
    for( dim_t j = max( im-1, 0 ); j < min( im+2, H+0 ); ++j )
      if ( i != r || j != im )
        rv.emplace(i,j);
  return rv;
}



template<typename T = uint8_t, uint8_t S = 10>
struct grid: public array<array<T, S>, S>
{
  using array<array<T,S>,S>::begin;
  using array<array<T,S>,S>::end;
  using array<array<T,S>,S>::operator[];

  T& operator[]( const pos_t p ){
    const auto [r, im] = p; 
    return operator[]( im )[ r ];
  }
  

  set<pos_t> operator++() {
    set<pos_t> rv;
    for_each( begin(), end(), 
        [&, i=-1]( array<T,S>& row ) mutable{
          ++i;
          for_each( row.begin(), row.end(), 
              [&, j=-1]( T& elm ) mutable { 
                ++j;
                if ( ++elm > 9 )
                  rv.emplace( j, i ); 
              });
        });
    return rv;
  }

  set<pos_t> flash( pos_t p ){
    set<pos_t> rv = neighbours<S,S>( p );
    erase_if( rv, 
        [&]( const pos_t p_ ) { return ++operator[]( p_ ) <= 9; });
    return rv;
  }

  set<pos_t> step() {
    set<pos_t> v = operator++(), s;
    while( !v.empty() )
    {
      set<pos_t> n;
      for_each( v.begin(), v.end(), 
          [&]( const auto p ){ n.merge( flash( p ) ); });
      s.merge( move(v) );
      v.clear();
      set_difference( n.begin(), n.end(), s.begin(), s.end(), 
          inserter(v, v.begin()));
    }
    
    for_each( s.begin(), s.end(), 
        [&]( const auto p ){ operator[]( p ) = 0; });
    return s;
  }

};

using grid_t = grid<uint8_t, 10>;

istream& operator<<( istream& in, grid_t& g )
{
  for( dim_t i=0; i<g.size(); ++i ){
    generate( g[i].begin(), g[i].end(), [&in]{return in.get() - '0';} );
    in.ignore();
  }
  return in;
}

ostream& operator<<( ostream& out, const grid_t& g )
{
  for( dim_t i=0; i<g.size(); ++i ){
    copy( g[i].begin(), g[i].end(), ostream_iterator<int>(out) );
    out << endl;
  }
  return out;
}

int main() {
  
  ifstream in("EG");
//  ifstream in("INPUT");
  grid_t C_; 


  for( uint8_t i=0; i<10; ++i ){
    generate( C_[i].begin(), C_[i].end(), [&in]{return in.get() - '0';} );
    in.ignore();
  }

//  cerr << C_ << endl;
//
//  C_.step();
//
//  cerr << C_ << endl;
//
//  C_.step();
//
//  cerr << C_ << endl;

  size_t f = 1;

  while( C_.step().size() != 100 )
    ++f;

  cout << f << endl;
}
